From: Carlos Garnacho <carlosg@gnome.org>
Date: Mon, 13 Nov 2023 12:17:08 +0100
Subject: backends/native: Make tablet devices "floating"

With the existing ClutterInputMode terminology (inherited from XI2),
hardware devices may be "physical" (i.e. attached to a logical device),
or "floating" (i.e. detached from all logical devices).

In the native backend, tablet devices are closer to "floating" than
"physical", since they do not emit events relative to the ClutterSeat
pointer logical device, nor drive the MetaCursorTracker sprite. This
is in contrast to X11 where all tablet devices drive the Virtual
Core Pointer by default, along with every other pointing device.

Change this mode in the Wayland backend to be more coherent. The
existing checks on the ClutterInputMode along Mutter seem appropriate
for handling these as floating devices, since they mainly care about
logical vs non-logical.

Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3393>
(cherry picked from commit 571acf57d1d15e5a6a9f77557bb217a1f391b524)
---
 src/backends/native/meta-input-device-native.c | 9 ++++++++-
 1 file changed, 8 insertions(+), 1 deletion(-)

diff --git a/src/backends/native/meta-input-device-native.c b/src/backends/native/meta-input-device-native.c
index 2f46d0a..530d6ce 100644
--- a/src/backends/native/meta-input-device-native.c
+++ b/src/backends/native/meta-input-device-native.c
@@ -1486,6 +1486,7 @@ meta_input_device_native_new_in_impl (MetaSeatImpl           *seat_impl,
   MetaInputDeviceNative *device;
   ClutterInputDeviceType type;
   ClutterInputCapabilities capabilities;
+  ClutterInputMode mode;
   char *vendor, *product;
   int n_rings = 0, n_strips = 0, n_groups = 1, n_buttons = 0;
   char *node_path;
@@ -1497,6 +1498,12 @@ meta_input_device_native_new_in_impl (MetaSeatImpl           *seat_impl,
   product = g_strdup_printf ("%.4x", libinput_device_get_id_product (libinput_device));
   node_path = g_strdup_printf ("/dev/input/%s", libinput_device_get_sysname (libinput_device));
 
+  if (libinput_device_has_capability (libinput_device,
+                                      LIBINPUT_DEVICE_CAP_TABLET_TOOL))
+    mode = CLUTTER_INPUT_MODE_FLOATING;
+  else
+    mode = CLUTTER_INPUT_MODE_PHYSICAL;
+
   if (libinput_device_has_capability (libinput_device,
                                       LIBINPUT_DEVICE_CAP_TABLET_PAD))
     {
@@ -1511,7 +1518,7 @@ meta_input_device_native_new_in_impl (MetaSeatImpl           *seat_impl,
                          "name", libinput_device_get_name (libinput_device),
                          "device-type", type,
                          "capabilities", capabilities,
-                         "device-mode", CLUTTER_INPUT_MODE_PHYSICAL,
+                         "device-mode", mode,
                          "vendor-id", vendor,
                          "product-id", product,
                          "n-rings", n_rings,
